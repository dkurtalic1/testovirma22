# Generalne napomene
- Na emulatoru ili uređaju na kojem testirate obavezno isključite animacije (inače testovi neće prolaziti)


![slika](animationoff.png)

- Importe iz testova ne smijete mijenjati. Ako su pogrešni promijenite strukturu projekta tako da:
- Naziv paketa projekta mora biti
```
    package ba.etf.rma22.projekat
```
- Modeli trebaju biti u paketu:
```
    ba.etf.rma22.projekat.data.models
```
- Repozitoriji trebaju biti u paketu:
```
    ba.etf.rma22.projekat.data.repositories
```
- Početna aktivnost se treba zvati:
```
    MainActivity
```

# Dependencies

    androidTestImplementation 'androidx.test.ext:junit:1.1.3'
    androidTestImplementation 'androidx.test:core-ktx:1.4.0'
    androidTestImplementation 'androidx.test.ext:junit-ktx:1.1.3'
    testImplementation("org.hamcrest:hamcrest:2.2")
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.4.0'
    androidTestImplementation 'androidx.test.espresso:espresso-intents:3.4.0'
    androidTestImplementation 'com.android.support.test.espresso:espresso-contrib:3.4.0'
